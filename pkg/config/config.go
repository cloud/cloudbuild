// Package config provides utilities for reading and storing configuration
// values.
package config

import (
	"os"
	"strings"
)

func isFalsy(s string) bool {
	s = strings.ToLower(s)
	return s == "false" || s == "no" || s == "0"
}

func isTruthy(s string) bool {
	s = strings.ToLower(s)
	return s == "true" || s == "yes" || s == "1"
}

// A Config holds all of the configuration values needed for the program.
type Config struct {
	CloudstackApiKey              string
	CloudstackSecretKey           string
	CloudstackApiBaseUrl          string
	CloudstackZoneName            string
	CloudstackServiceOfferingName string
	CloudstackKeypairName         string
	SSHKeyPath                    string
	// DISTROS_TO_CHECK must be a comma-separated list of keys of the
	// distrosInfo map in cloudbuilder.go. Example: "debian,ubuntu".
	DistrosToCheck []string
	// If SKIP_FRESHNESS_CHECK is set to a truthy value, then existing
	// templates in CloudStack will not be checked to see if they are
	// outdated. This should only be used during development.
	// Default: false.
	SkipFreshnessCheck bool
	// If SKIP_DOWNLOAD is set to a truthy value, then QCOW2 images will
	// not be downloaded and are assumed to be already be present in the
	// current working directory. Default: false.
	SkipDownload bool
	// If DELETE_OLD_TEMPLATES is set to falsy value, then old templates
	// for a distro will not be deleted after a new one is uploaded.
	// Default: true.
	DeleteOldTemplates bool
	UploadDirectory    string
	UploadBaseUrl      string
	MirrorHost         string
	EmailServer        string
	EmailSender        string
	EmailSenderName    string
	EmailRecipient     string
	EmailReplyTo       string
}

// New returns a Config filled with values read from environment variables.
// It panics if a required environment variable is empty or not set.
func New() *Config {
	cfg := &Config{
		CloudstackApiKey:    os.Getenv("CLOUDSTACK_API_KEY"),
		CloudstackSecretKey: os.Getenv("CLOUDSTACK_SECRET_KEY"),
		SSHKeyPath:          os.Getenv("SSH_KEY_PATH"),
		UploadDirectory:     os.Getenv("UPLOAD_DIRECTORY"),
		UploadBaseUrl:       os.Getenv("UPLOAD_BASE_URL"),
		EmailRecipient:      os.Getenv("EMAIL_RECIPIENT"),
		DeleteOldTemplates:  true,
	}
	if cfg.CloudstackApiKey == "" {
		panic("CLOUDSTACK_API_KEY is empty or not set")
	}
	if cfg.CloudstackSecretKey == "" {
		panic("CLOUDSTACK_SECRET_KEY is empty or not set")
	}
	if cfg.SSHKeyPath == "" {
		panic("SSH_KEY_PATH is empty or not set")
	}
	if val, ok := os.LookupEnv("DISTROS_TO_CHECK"); ok {
		cfg.DistrosToCheck = strings.Split(val, ",")
	} else {
		panic("DISTROS_TO_CHECK is not set")
	}
	if cfg.UploadDirectory == "" {
		panic("UPLOAD_DIRECTORY is empty or not set")
	}
	if cfg.UploadBaseUrl == "" {
		panic("UPLOAD_BASE_URL is empty or not set")
	}
	if cfg.EmailRecipient == "" {
		panic("EMAIL_RECIPIENT is empty or not set")
	}

	if isTruthy(os.Getenv("SKIP_FRESHNESS_CHECK")) {
		cfg.SkipFreshnessCheck = true
	}
	if isTruthy(os.Getenv("SKIP_DOWNLOAD")) {
		cfg.SkipDownload = true
	}
	if isFalsy(os.Getenv("DELETE_OLD_TEMPLATES")) {
		cfg.DeleteOldTemplates = false
	}

	// These should never change
	cfg.CloudstackApiBaseUrl = "https://cloud.csclub.uwaterloo.ca/client/api"
	cfg.CloudstackZoneName = "Zone1"
	cfg.CloudstackServiceOfferingName = "Small Instance"
	cfg.CloudstackKeypairName = "management-keypair"
	cfg.MirrorHost = "mirror.csclub.uwaterloo.ca"
	cfg.EmailServer = "mail.csclub.uwaterloo.ca:25"
	cfg.EmailSender = "cloudbuild@csclub.uwaterloo.ca"
	cfg.EmailSenderName = "cloudbuild"
	cfg.EmailReplyTo = "no-reply@csclub.uwaterloo.ca"
	return cfg
}
