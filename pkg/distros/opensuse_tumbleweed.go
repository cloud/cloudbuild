package distros

import (
	"errors"
	"fmt"

	"github.com/rs/zerolog/log"
	"libguestfs.org/guestfs"

	"git.csclub.uwaterloo.ca/cloud/cloudbuild/pkg/config"
)

type OpensuseTumbleweedTemplateManager struct {
	TemplateManager
}

func NewOpensuseTumbleweedTemplateManager(cfg *config.Config) *OpensuseTumbleweedTemplateManager {
	logger := log.With().Str("distro", "opensuse-tumbleweed").Logger()
	opensuseTumbleweedTemplateManager := OpensuseTumbleweedTemplateManager{
		TemplateManager{
			cfg:    cfg,
			logger: &logger,
			impl:   nil,
		},
	}
	opensuseTumbleweedTemplateManager.TemplateManager.impl = &opensuseTumbleweedTemplateManager
	return &opensuseTumbleweedTemplateManager
}

func (mgr *OpensuseTumbleweedTemplateManager) GetLatestVersion() (version string, codename string, err error) {
	err = errors.New("rolling release")
	return
}

func (mgr *OpensuseTumbleweedTemplateManager) DownloadTemplate(version, codename string) (path string, err error) {
	return mgr.DownloadTemplateGeneric(
		"openSUSE-Tumbleweed-Minimal-VM.x86_64-Cloud.qcow2",
		"https://download.opensuse.org/tumbleweed/appliances/openSUSE-Tumbleweed-Minimal-VM.x86_64-Cloud.qcow2",
	)
}

func (mgr *OpensuseTumbleweedTemplateManager) addSystemUser(handle *guestfs.Guestfs, name, comment string) error {
	matches, err := handle.Grep("^"+name, "/etc/passwd", nil)
	if err != nil {
		return fmt.Errorf("Could not grep /etc/passwd: %w", err)
	}
	if len(matches) > 0 {
		return nil
	}
	_, err = mgr.logAndRunCommand(handle, []string{
		"useradd",
		"--system",
		"--no-create-home",
		"--home-dir", "/",
		"--shell", "/usr/sbin/nologin",
		"--comment", comment,
		name,
	})
	if err != nil {
		return fmt.Errorf("Could not add user %s: %w", name, err)
	}
	return nil
}

func (mgr *OpensuseTumbleweedTemplateManager) InstallSystemdResolved(handle *guestfs.Guestfs) error {
	// This package contains both systemd-networkd and systemd-resolved
	args := []string{"zypper", "--non-interactive", "install", "systemd-network"}
	if _, err := mgr.logAndRunCommand(handle, args); err != nil {
		return fmt.Errorf("Could not install systemd-network: %w", err)
	}
	// The systemd-network and systemd-resolve users don't get added for
	// some reason, even though the systemd services need to run as them
	if err := mgr.addSystemUser(handle, "systemd-resolve", "systemd Resolver"); err != nil {
		return err
	}
	if err := mgr.addSystemUser(handle, "systemd-network", "systemd Network Management"); err != nil {
		return err
	}
	// nsswitch.conf doesn't get updated either
	args = []string{
		"sed",
		"-i",
		`s/^hosts:/hosts:\t\tfiles myhostname resolve [!UNAVAIL=return] dns/`,
		"/usr/etc/nsswitch.conf",
	}
	if _, err := mgr.logAndRunCommand(handle, args); err != nil {
		return fmt.Errorf("Could not update nsswitch.conf: %w", err)
	}
	return nil
}

func (mgr *OpensuseTumbleweedTemplateManager) removeWelcomeMessage(handle *guestfs.Guestfs) error {
	// Says "Have a lot of fun..."
	mgr.logger.Debug().Msg("Removing /usr/lib/motd.d/welcome")
	return handle.Rm_f("/usr/lib/motd.d/welcome")
}

func (mgr *OpensuseTumbleweedTemplateManager) CommandToUpdatePackageCache() []string {
	return []string{"sudo", "zypper", "refresh", "--force"}
}

func (mgr *OpensuseTumbleweedTemplateManager) PerformDistroSpecificModifications(handle *guestfs.Guestfs) error {
	// Newer Tumbleweed images shouldn't be using wickedd anymore
	wickeddExists, err := handle.Is_file("/lib/systemd/system/wickedd.service", nil)
	if err != nil {
		return err
	}
	if wickeddExists {
		return errors.New("wickedd.service was unexpectedly found")
	}
	return mgr.removeWelcomeMessage(handle)
}
