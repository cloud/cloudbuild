#!/bin/bash

set -ex

ip -brief link show | while read interface _; do
    if [ $interface = lo ]; then
	continue
    fi
    sysctl net.ipv6.conf.$interface.accept_ra=0
    sysctl net.ipv6.conf.$interface.accept_dad=0
done

ip -6 -brief addr show dynamic | \
    while read iface _ addrs; do
        for addr in $addrs; do
            ip addr del dev $iface $addr
        done
    done

ip -6 route show proto ra | grep -E -o "^default via [a-f0-9:]+ dev [a-z0-9]+" | \
    while read line; do
        ip route del $line
    done
