#!/bin/bash

set -ex

nmcli -f name -terse con show --active | \
    while read conn_name; do
        if ! [[ "$conn_name" =~ ^cloud-init\ e ]]; then
            continue
        fi
        iface=$(echo "$conn_name" | cut -d ' ' -f 2)
        nmcli c modify "$conn_name" ipv6.method link-local
        nmcli d reapply $iface
    done

ip -6 -brief addr show dynamic | \
    while read iface _ addrs; do
        for addr in $addrs; do
            ip addr del dev $iface $addr
        done
    done

ip -6 route show proto ra | grep -E -o "^default via [a-f0-9:]+ dev [a-z0-9]+" | \
    while read line; do
        ip route del $line
    done
