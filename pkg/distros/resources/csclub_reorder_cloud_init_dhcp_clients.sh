#!/bin/bash

set -e

for file in /usr/lib/python3.*/site-packages/cloudinit/net/dhcp.py; do
    if grep -q '^ALL_DHCP_CLIENTS = \[Dhcpcd, IscDhclient' "$file"; then
        echo "Overwriting $file"
        sed -i 's/^ALL_DHCP_CLIENTS = \[Dhcpcd, IscDhclient/ALL_DHCP_CLIENTS = [IscDhclient, Dhcpcd/' "$file"
    fi
done
