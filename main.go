package main

import (
	"os"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"git.csclub.uwaterloo.ca/cloud/cloudbuild/pkg/cloudbuilder"
	"git.csclub.uwaterloo.ca/cloud/cloudbuild/pkg/config"
)

func setupLogging() {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr, TimeFormat: time.RFC3339})
}

func main() {
	setupLogging()
	cfg := config.New()
	builder := cloudbuilder.New(cfg)
	if err := builder.Start(); err != nil {
		panic(err)
	}
}
