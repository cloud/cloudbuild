DEPS_DIR = guestfs/deps
LIBRARY_PATH = $(DEPS_DIR)/usr/lib/x86_64-linux-gnu:$(DEPS_DIR)/lib/x86_64-linux-gnu
LIBGUESTFS_PATH = guestfs/appliance
LIBGUESTFS_HV = scripts/qemu.sh
APPLIANCE_VERSION = 1.46.0

ifeq ($(shell test -e $(DEPS_DIR)/usr/lib/x86_64-linux-gnu/libvirt.so.0 && echo -n yes),yes)
	CGO_LDFLAGS = -lvirt -lyajl -l:libssh.so.4
endif

# Export LIBGUESTFS_DEBUG=1 to debug

all:
	LIBRARY_PATH=$(LIBRARY_PATH) CGO_LDFLAGS="$(CGO_LDFLAGS)" go build

run:
	LD_LIBRARY_PATH=$(LIBRARY_PATH) LIBGUESTFS_PATH=$(LIBGUESTFS_PATH) LIBGUESTFS_HV=$(LIBGUESTFS_HV) ./cloudbuild

guestfish:
	LD_LIBRARY_PATH=$(LIBRARY_PATH) LIBGUESTFS_PATH=$(LIBGUESTFS_PATH) LIBGUESTFS_HV=$(LIBGUESTFS_HV) scripts/guestfish.sh

deps:
	scripts/download-deps.sh
	scripts/create-libguestfs-module.sh

guestfish-deps:
	scripts/download-guestfish-deps.sh

appliance-download:
	mkdir -p guestfs
	cd guestfs && \
		rm -rf appliance && \
		wget https://download.libguestfs.org/binaries/appliance/appliance-$(APPLIANCE_VERSION).tar.xz && \
		tar Jxvf appliance-$(APPLIANCE_VERSION).tar.xz && \
		rm appliance-$(APPLIANCE_VERSION).tar.xz

appliance:
	scripts/create-appliance.sh

supermin-download:
	mkdir -p guestfs
	[ -e /usr/bin/supermin ] || cd guestfs && apt download supermin && dpkg -x supermin_*.deb deps && rm supermin_*.deb

prune-deps:
	rm -rf $(DEPS_DIR)/usr/share $(DEPS_DIR)/usr/include $(DEPS_DIR)/etc

clean:
	rm -rf cloudbuild guestfs

.PHONY: all run guestfish deps guestfish-deps appliance-download appliance supermin-download clean
