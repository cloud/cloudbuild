#!/bin/bash

set -ex

DEPS_DIR=
if ! [ -d /usr/share/gocode/src/libguestfs.org/guestfs ]; then
    DEPS_DIR=guestfs/deps
fi
mkdir -p guestfs
cp $DEPS_DIR/usr/share/gocode/src/libguestfs.org/guestfs/guestfs.go guestfs/
cp $DEPS_DIR/usr/include/guestfs.h guestfs/
cd guestfs
rm -f go.mod
go mod init guestfs
