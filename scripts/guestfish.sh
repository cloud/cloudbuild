#!/bin/bash

DEPS_DIR=guestfs/deps
GUESTFISH=/usr/bin/guestfish
if ! [ -e $GUESTFISH ]; then
    GUESTFISH=${DEPS_DIR}${GUESTFISH}
fi
if ! [ -e /usr/bin/qemu-img ]; then
    export PATH="$DEPS_DIR/usr/bin:$PATH"
fi
set -x
exec $GUESTFISH --network
