#!/bin/bash

set -e

core_dependencies=(
    golang-guestfs-dev
    libguestfs0
    libguestfs-dev
)

# TODO: automatically generate this list from the dependencies for
# golang-guestfs-dev
libvirt_dependencies=(
    libbrlapi0.8
    libcacard0
    libcapstone4
    libdaxctl1
    libfdt1
    libndctl6
    libpmem1
    libslirp0
    libspice-server1
    libssh-4
    libusbredirparser1
    libvdeplug2
    libvirglrenderer1
    libvirt0
    libvirt-dev
    libxencall1
    libxendevicemodel1
    libxenevtchn1
    libxenforeignmemory1
    libxengnttab1
    libxentoolcore1
    libxentoollog1
    libyajl2
    libyajl-dev
    qemu-system-x86
    seabios
)

. /etc/os-release
if [ "$VERSION_CODENAME" = bullseye ]; then
    dependencies+=(liburing1 libxenmisc4.14 libxenstore3.0)
elif [ "$VERSION_CODENAME" = bookworm ]; then
    dependencies+=(liburing2 libxenmisc4.17 libxenstore4)
else
    echo "This script does not support $VERSION_CODENAME yet" >&2
    exit 1
fi

dependencies=("${core_dependencies[@]}" "${libvirt_dependencies[@]}")

is_installed() {
    status=$(dpkg-query --show --showformat '${db:Status-Status}' "$1" 2>/dev/null)
    [ $? -eq 0 ] && [ "$status" = installed ]
}

DEPS_DIR=guestfs/deps
mkdir -p $DEPS_DIR
cd guestfs
for dep in "${dependencies[@]}"; do
    # Even if a package is already installed, another package which we
    # download might have a relative symlink to one of its files; download
    # everything just to be safe
    #if is_installed $dep; then
    #    continue
    #fi
    set -x
    apt download $dep
    dpkg -x ${dep}_*.deb deps
    set +x
done
set -x
rm -f *.deb
