#!/bin/sh

QEMU=/usr/bin/qemu-system-x86_64
if [ -x $QEMU ]; then
    exec $QEMU "$@"
fi
DEPS_DIR=guestfs/deps
exec ${DEPS_DIR}${QEMU} -L $DEPS_DIR/usr/share/seabios "$@"
