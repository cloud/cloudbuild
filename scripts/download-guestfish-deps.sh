#!/bin/bash

dependencies=(
    libguestfs-tools
    libconfig9
    qemu-utils
)

is_installed() {
    status=$(dpkg-query --show --showformat '${db:Status-Status}' "$1" 2>/dev/null)
    [ $? -eq 0 ] && [ "$status" = installed ]
}

mkdir -p guestfs/deps
cd guestfs
for dep in "${dependencies[@]}"; do
    if is_installed $dep; then
        continue
    fi
    set -x
    apt download $dep
    dpkg -x ${dep}_*.deb deps
    set +x
done
set -x
rm -f *.deb
