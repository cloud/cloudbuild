#!/bin/bash

set -e
DEPS_DIR=guestfs/deps
SUPERMIN=/usr/bin/supermin
INPUT_DIR=/usr/lib/x86_64-linux-gnu/guestfs/supermin.d
if ! [ -e $SUPERMIN ]; then
    SUPERMIN=${DEPS_DIR}${SUPERMIN}
fi
if ! [ -e $INPUT_DIR ]; then
    INPUT_DIR=${DEPS_DIR}${INPUT_DIR}
fi

set -x
mkdir -p /var/tmp/.guestfs-`id -u`
$SUPERMIN --build --verbose --if-newer \
    --lock /var/tmp/.guestfs-`id -u`/lock \
    --copy-kernel \
    -f ext2 \
    --host-cpu x86_64 \
    $INPUT_DIR \
    -o /var/tmp/.guestfs-`id -u`/appliance.d
mkdir -p guestfs
rm -rf guestfs/appliance
mv /var/tmp/.guestfs-`id -u`/appliance.d guestfs/appliance
touch guestfs/appliance/README.fixed
