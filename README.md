# cloudbuild
cloudbuild downloads, modifies and uploads VM templates for the CSC cloud
(CloudStack) using publicly available images for common Linux distros.

## Installing dependencies
Run the following in a Debian Docker/Podman container.
```sh
apt install golang-guestfs-dev
scripts/create-libguestfs-module.sh
```

### Optional dependencies
If you are developing cloudbuild, you may wish to install
[guestfish](https://libguestfs.org/guestfish.1.html), an interactive shell
for guestfs:
```sh
apt install libguestfs-tools
```

To run:
```sh
make guestfish

><fs> add /path/to/your/template.img
><fs> run
><fs> inspect-os
><fs> # use the output from inspect-os to find the root partition
><fs> mount /dev/sda4 /
```

## Creating the appliance
Next you need an appliance, which is a kernel + initrd + rootfs used by the
VM created by guestfs. You have two options:

1. Download an appliance:
   ```sh
   make appliance-download
   ```
**OR**

2. Create an appliance using the host's kernel:
   ```sh
   make supermin-download
   make appliance
   ```

## Building the program
```sh
make
```

## Running the program
Make sure the following environment variables are set:
```sh
# these values must be obtained from the CloudStack web UI
export CLOUDSTACK_API_KEY=secret
export CLOUDSTACK_SECRET_KEY=secret
# set these to the distros for which cloudbuild will create templates
export DISTROS_TO_CHECK=ubuntu,fedora
# the modified templates will be temporarily moved here so that CloudStack
# can download them
export UPLOAD_DIRECTORY=/var/www/csc-cloud-images
# the modified templates (in the UPLOAD_DIRECTORY) must be downloadable
# from this base URL
export UPLOAD_BASE_URL=http://biloba.csclub.uwaterloo.ca/csc-cloud-images
# notification emails will be sent here
export EMAIL_RECIPIENT=root@csclub.uwaterloo.ca
# path of SSH key used to SSH into VMs created in CloudStack
export SSH_KEY_PATH=~/.ssh/id_rsa_cloudstack
```

Finally:
```sh
make run
```
