module git.csclub.uwaterloo.ca/cloud/cloudbuild

go 1.17

replace libguestfs.org/guestfs => ./guestfs

require (
	github.com/rs/zerolog v1.26.1
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d
	libguestfs.org/guestfs v0.0.0-00010101000000-000000000000
)
